package com.paymeasap.service;

import com.paymeasap.model.ResourceMeta;
import com.paymeasap.model.Transaction;
import com.paymeasap.model.TransactionInput;

import java.util.List;

public interface TransactionService {
    List<Transaction> transactions();

    Transaction transaction(String transactionId);

    ResourceMeta create(TransactionInput transactionInput);

    boolean delete(String transactionId);

}
