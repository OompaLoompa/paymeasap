package com.paymeasap.service;

import com.paymeasap.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<Transaction> transactions(String userId);
}
