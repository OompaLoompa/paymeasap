package com.paymeasap.service;

import com.paymeasap.model.ResourceMeta;
import com.paymeasap.model.Transaction;
import com.paymeasap.model.TransactionInput;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Override
    public List<Transaction> transactions() {
        return Collections.emptyList();
    }

    @Override
    public Transaction transaction(String transactionId) {
        return null;
    }

    @Override
    public ResourceMeta create(TransactionInput transactionInput) {
        return null;
    }

    @Override
    public boolean delete(String transactionId) {
        return false;
    }
}
