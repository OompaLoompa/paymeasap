package com.paymeasap.controller;

import com.paymeasap.model.PayMeAsapResponse;
import com.paymeasap.model.ResourceMeta;
import com.paymeasap.model.Transaction;
import com.paymeasap.model.TransactionInput;
import com.paymeasap.service.TransactionService;
import lombok.extern.log4j.Log4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public PayMeAsapResponse<List<Transaction>> getTransactionsForUser() {
        log.debug("Getting all the transactions");
        List<Transaction> transactions = transactionService.transactions();
        return PayMeAsapResponse.newInstance(transactions);
    }

    @GetMapping("/{transactionId}")
    public PayMeAsapResponse<Transaction> get(@PathVariable("transactionId") String transactionId) {
        log.debug("Fetching transaction with id: " + transactionId);
        Transaction transaction = transactionService.transaction(transactionId);
        return PayMeAsapResponse.newInstance(transaction);
    }

    @PostMapping
    public PayMeAsapResponse<ResourceMeta> create(@Valid @RequestBody TransactionInput transactionInput) {
        log.debug("Creating new transaction with following input: " + transactionInput);
        ResourceMeta resourceMeta = transactionService.create(transactionInput);
        return PayMeAsapResponse.newInstance(resourceMeta);
    }

    @DeleteMapping("/{transactionId}")
    public ResponseEntity delete(@PathVariable("transactionId") String transactionId) {
        log.debug("Deleting transaction with following id: " + transactionId);
        return transactionService.delete(transactionId) ?
                ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}
