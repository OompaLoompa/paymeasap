package com.paymeasap.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Transaction {
    private String transactionId;
    private User user;
    private float amount;

    public Transaction(User user, float amount) {
        this.user = user;
        this.amount = amount;
    }

    public Transaction transactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }
}
