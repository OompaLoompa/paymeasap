package com.paymeasap.model;

import lombok.Data;

@Data
public class PayMeAsapResponse<T> {

    T data;

    public PayMeAsapResponse(T data) {
        this.data = data;
    }

    public static <T> PayMeAsapResponse newInstance(T transactions) {
        return new PayMeAsapResponse(transactions);
    }
}
