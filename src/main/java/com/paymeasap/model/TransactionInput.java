package com.paymeasap.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class TransactionInput {

    @NotNull
    private User user;

    @NotNull
    private Float amount;

    public TransactionInput(User user, float amount) {
        this.user = user;
        this.amount = amount;
    }
}
