package com.paymeasap.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paymeasap.model.ResourceMeta;
import com.paymeasap.model.Transaction;
import com.paymeasap.model.TransactionInput;
import com.paymeasap.model.User;
import com.paymeasap.service.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionControllerTest {

    private MockMvc mockMvc;

    @Mock
    TransactionService transactionService;

    @InjectMocks
    TransactionController transactionController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(transactionController).alwaysDo(print()).build();
    }

    @Test
    public void itShouldFetchAllTheTransactions() throws Exception {
        // Given
        User user = new User("John", "Doe");
        float amount = 45.0F;
        Transaction transaction = new Transaction(user, amount);
        given(transactionService.transactions()).willReturn(Arrays.asList(transaction));

        // When
        this.mockMvc.perform(get("/transaction"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").isArray())
                .andExpect(jsonPath("$.data[0].user.firstName").value("John"))
                .andExpect(jsonPath("$.data[0].amount").value(45.0F));
    }

    @Test
    public void itShouldGetTransactionById() throws Exception {
        // Given
        String transactionId = "unique-trans-id";
        User user = new User("John", "Doe");
        float amount = 45.0F;
        Transaction transaction = new Transaction(user, amount).transactionId(transactionId);
        given(transactionService.transaction(anyString())).willReturn(transaction);

        // When
        mockMvc.perform(get("/transaction/" + transactionId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.transactionId").value(transactionId));
    }

    @Test
    public void itShouldCreateNewTransaction() throws Exception {
        // Given
        String transactionId = "unique-trans-id";
        User user = new User("John", "Doe");
        float amount = 45.0F;
        TransactionInput transactionInput = new TransactionInput(user, amount);
        ResourceMeta resourceMeta = new ResourceMeta("/transaction/" + transactionId);
        given(transactionService.create(transactionInput)).willReturn(resourceMeta);

        // When
        mockMvc.perform(post("/transaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(transactionInput)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.query").value("/transaction/" + transactionId));
    }

    private byte[] asJsonString(Object anyObj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsBytes(anyObj);
    }

    @Test
    public void itShouldDeleteTransaction() throws Exception {
        // Given
        String transactionId = "unique-trans-id";
        given(transactionService.delete(anyString())).willReturn(true);

        // When
        mockMvc.perform(delete("/transaction/" + transactionId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void itShouldGive404StatusForCallingDeleteSecondTimeWithSameTransactionId() throws Exception {
        // Given
        String transactionId = "unique-trans-id";
        given(transactionService.delete(anyString())).willReturn(false);

        // When
        mockMvc.perform(delete("/transaction/" + transactionId))
                .andExpect(status().isNotFound());
    }
}
